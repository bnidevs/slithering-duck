from . import Command
from .. import utils

import discord
import json
import random
import re
from fractions import Fraction

## prepare query str -- we want to load balance between query1 and query2 to be good netizens
randomQuery = random.choice((1, 2))

queryurl = (
    "https://query" + str(randomQuery) + ".finance.yahoo.com/v7/finance/quote?symbols="
)

stonksurl = "https://emoji.gg/assets/emoji/9783_Reddit_stonksUP.png"
notstonksurl = "https://emoji.gg/assets/emoji/7499_Reddit_stonksDOWN.png"
wsburl = "https://i.pinimg.com/originals/29/24/89/292489e7d0bf8ce7d5ffd81be62d0800.png"

def float_beautifier(num, digits=2):
    return "$" + ("{0:,." + str(digits) + "f}").format(num)


async def get_stock(stonk):
    if type(stonk) is list:
        stonk = ",".join(stonk)
    async with utils.get_aiohttp().get(queryurl + stonk) as stock_request:
        if stock_request.status != 200:
            stock_request_text = await stock_request.text()
            return False, stock_request_text
        stonkinfo = json.loads(await stock_request.read())["quoteResponse"]["result"]
    return True, stonkinfo


def check_crypto(stonk):
    if stonk["quoteType"] == "CRYPTOCURRENCY" and stonk["currency"] != "USD":
        return False, "Crypto not in USD"
    return True, None


def check_market_open(stonk):
    marketopen = stonk["marketState"]
    if not stonk["quoteType"] == "CRYPTOCURRENCY" and marketopen == "CLOSED":
        return False, "Market not open"
    return True, None


def check_valid_stock(stonk):
    if "regularMarketPrice" not in stonk or stonk["quoteType"] == "CURRENCY" or stonk["regularMarketPrice"] == 0.0:
        return False, "Error: " + stonk["symbol"] + " cannot purchase this."
    return True, None

def get_price(stonk):
    marketopen = stonk["marketState"]
    if marketopen == "POST" or marketopen == "PRE" or marketopen == "CLOSED" and (not stonk["quoteType"] == "CRYPTOCURRENCY"):
        try:
            return stonk["postMarketPrice"]
        except KeyError:
            return stonk["regularMarketPrice"]
    return stonk["regularMarketPrice"]

def check_all(stonk):
    validstock, errmsg = check_valid_stock(stonk)
    if not validstock:
        return validstock, errmsg
    validcrypto, errmsg = check_crypto(stonk)
    if not validcrypto:
        return validcrypto, errmsg
    marketopen, errmsg = check_market_open(stonk)
    if not marketopen:
        return marketopen, errmsg
    return True, None


def parse_share_amt(numshares):
    mxamt = 0
    dollar = False

    fraction = re.findall(f"^(\d+?)\/(\d+?)$", numshares)

    if numshares.lower() == "max":
        mxamt = 1
    elif "nan" in numshares.lower() or "inf" in numshares.lower():
        return (
            False,
            mxamt,
            dollar,
            "Invalid " + ("dollar amount" if dollar else "number of shares"),
        )
    elif len(fraction) == 1:
        numerator = int(fraction[0][0])
        denominator = int(fraction[0][1])
        if (
            numerator <= 0
            or denominator <= 0
            or numerator > denominator
            or float(Fraction(numerator, denominator)) <= 0
        ):
            return (
                False,
                mxamt,
                dollar,
                "Invalid fraction of shares purchased. Fraction must be > 0 and <= 1. Also numerator and denominator must be positive integers.",
            )
        mxamt = float(Fraction(numerator, denominator))
        print(mxamt)
    else:
        if numshares[0] == "$":
            dollar = True
            numshares = numshares[1:]
        try:
            numshares = float(numshares)
            if numshares <= 0:
                raise ValueError("")
            return True, mxamt, dollar, numshares
        except ValueError:
            return (
                False,
                mxamt,
                dollar,
                "Invalid " + ("dollar amount" if dollar else "number of shares"),
            )
    return True, mxamt, dollar, numshares


def get_existing_shares(cursor, user_id, stonk):
    cursor.execute(
        "SELECT num, cost FROM paper_traders WHERE user_id=? AND stock=?",
        (
            user_id,
            stonk,
        ),
    )

    existing = cursor.fetchone()
    if existing == None:
        return None
    existing = list(existing)
    return [float(t) for t in existing]


def get_all_shares(cursor, user_id=None):
    if user_id == None:
        cursor.execute("SELECT * FROM paper_traders")
    else:
        cursor.execute("SELECT * FROM paper_traders WHERE user_id=?", (user_id,))
    stocks = cursor.fetchall()
    if len(stocks) == 0:
        return []
    return stocks


def insert_into_table(cursor, tpl):  # tpl = (user_id, ticker, numshares, cost)
    cursor.execute(
        "INSERT INTO paper_traders VALUES(?,?,?,?)",
        tpl,
    )


def update_shares(cursor, tpl):  # tpl = (numshares, cost, user_id, ticker)
    cursor.execute(
        "UPDATE paper_traders SET num=?, cost=? WHERE user_id=? AND stock=?",
        tpl,
    )


def clear_shares(cursor, user_id, stonk):
    cursor.execute(
        "DELETE FROM paper_traders WHERE user_id=? AND stock=?",
        (
            user_id,
            stonk,
        ),
    )


def get_embed(dict, icon=wsburl, lim=0, input_title=None, input_footer=None):
    if input_title == None:
        rtrnmsg = discord.Embed()
    else:
        rtrnmsg = discord.Embed(title=input_title)
    if lim > 0:
        i = 0
    for k in dict:
        if lim > 0 and i >= lim:
            break
        rtrnmsg.add_field(
            name=k,
            value=dict[k],
            inline=False,
        )
    if input_footer != None:
        rtrnmsg.set_footer(text=input_footer)
    rtrnmsg.set_thumbnail(url=icon)
    return rtrnmsg


class StockError(Exception):
    pass


class Trading(Command):
    names = ["trade", "trading"]
    description = "Paper trading function for Rubber Duck"
    usage = "!trade buy AMT SYM / sell AMT SYM / net / portfolio / cash / rank"
    examples = "!trade buy 3 GME / !trade sell 3 GME / !trade net / !trade portfolio / !trade cash / !trade rank"
    causes_spam = True

    async def execute_command(self, client, msg, content, **kwargs):
        global_dollar = False
        global_mxamt = 0
        global_user_id = msg.author.id
        global_crypto = False

        try:
            if len(content.strip()) == 0:
                raise StockError("Usage: " + self.usage)
            sep_contents = content.strip().split(" ")

            if sep_contents[0].lower() == "buy":

                # PARSING

                if len(sep_contents) != 3:
                    raise StockError("Usage: " + self.usage)
                ticker = sep_contents[2].upper()
                numshares = sep_contents[1].lower()
                valid_numshares, mxamt, dollar, parsed = parse_share_amt(numshares)
                if not valid_numshares:
                    raise StockError(parsed)
                global_mxamt = mxamt
                global_dollar = dollar
                numshares = parsed

                # API QUERY

                apistatus, stonkinfo = await get_stock(ticker)
                if not apistatus:
                    raise StockError(
                        "Failed to retrieve stock information: " + stonkinfo
                    )
                if len(stonkinfo) == 0:
                    raise StockError(
                        "No stock information found (are those valid stocks?)."
                    )
                stonk = stonkinfo[0]

                # VALIDATE

                validstock, errmsg = check_all(stonk)
                if not validstock:
                    raise StockError(errmsg)
                if stonk["quoteType"] == "CRYPTOCURRENCY":
                    global_crypto = True
                # DB QUERY CASH

                cash = get_existing_shares(client.c, global_user_id, "MONEY")
                if cash == None:
                    cash = 100000.0
                    insert_into_table(
                        client.c,
                        (
                            global_user_id,
                            "MONEY",
                            cash,
                            0.0,
                        ),
                    )
                else:
                    cash = cash[0]
                # SHARE RECALCULATION

                price = float(get_price(stonk))

                if global_mxamt:
                    cost = cash * global_mxamt
                    numshares = cost / price
                elif global_dollar:
                    cost = numshares
                    numshares = cost / price
                else:
                    cost = price * numshares
                # CHECK AFFORD

                if cash < cost:
                    raise StockError("Not enough money")
                # PROCESS

                cash -= cost

                # DB WRITE STOCK

                existing = get_existing_shares(client.c, global_user_id, ticker)
                if existing == None:
                    insert_into_table(
                        client.c,
                        (
                            global_user_id,
                            ticker,
                            0.0 + numshares,
                            cost / numshares,
                        ),
                    )
                else:
                    existing[1] = (existing[0] * existing[1] + cost) / (numshares + existing[0])
                    existing[0] += numshares
                    update_shares(
                        client.c,
                        (
                            0.0 + existing[0],
                            existing[1],
                            global_user_id,
                            ticker,
                        ),
                    )
                # DB WRITE CASH

                update_shares(
                    client.c,
                    (
                        cash,
                        0.0,
                        global_user_id,
                        "MONEY",
                    ),
                )

                # DB COMMIT

                client.connection.commit()

                # REPLY MSG

                embeddict = {
                    "Buy Successful": ticker,
                    "Shares": str(numshares),
                    "Price": float_beautifier(price, 8 if global_crypto else 2),
                    "Total Cost": float_beautifier(cost, 8 if global_crypto else 2),
                    "Trader": msg.author.mention,
                }
                rtrnmsg = get_embed(embeddict)
                return await utils.delay_send(msg.channel, embed=rtrnmsg)
            elif sep_contents[0].lower() == "sell":

                # PARSING

                if len(sep_contents) != 3:
                    raise StockError("Usage: " + self.usage)
                ticker = sep_contents[2].upper()
                numshares = sep_contents[1].lower()
                valid_numshares, mxamt, dollar, parsed = parse_share_amt(numshares)
                if not valid_numshares:
                    raise StockError(parsed)
                global_mxamt = mxamt
                global_dollar = dollar
                numshares = parsed

                # API QUERY

                apistatus, stonkinfo = await get_stock(ticker)
                if not apistatus:
                    raise StockError(
                        "Failed to retrieve stock information: " + stonkinfo
                    )
                if len(stonkinfo) == 0:
                    raise StockError(
                        "No stock information found (are those valid stocks?)."
                    )
                stonk = stonkinfo[0]

                # VALIDATE

                validstock, errmsg = check_all(stonk)
                if not validstock:
                    raise StockError(errmsg)
                if stonk["quoteType"] == "CRYPTOCURRENCY":
                    global_crypto = True
                # DB QUERY STOCK

                curr_holdings = get_existing_shares(
                    client.c,
                    global_user_id,
                    ticker,
                )
                if curr_holdings == None:
                    raise StockError("Not enough shares")
                # SHARE RECALCULATION

                avgcost = curr_holdings[1]
                curr_holdings = curr_holdings[0]
                price = float(get_price(stonk))
                if global_mxamt:
                    numshares = curr_holdings * global_mxamt
                    cost = numshares * price
                elif global_dollar:
                    cost = numshares
                    numshares = cost / price
                else:
                    cost = price * numshares
                # CHECK STOCK

                if curr_holdings < numshares:
                    raise StockError("Not enough shares")
                # DB QUERY CASH

                cash = get_existing_shares(client.c, global_user_id, "MONEY")[0]
                cash += cost  # no need to error handle None here because you have to have stock (a database entry) in order to sell

                # DB WRITE CASH

                update_shares(
                    client.c,
                    (
                        cash,
                        0.0,  # this table row holds user cash so no need to store cost
                        global_user_id,
                        "MONEY",
                    ),
                )

                # PROCESS

                curr_holdings -= numshares
                diff = cost - (avgcost * numshares)

                # DB WRITE STOCK

                if curr_holdings == 0:
                    clear_shares(client.c, global_user_id, ticker)
                else:
                    update_shares(
                        client.c,
                        (
                            0.0 + curr_holdings,
                            avgcost,
                            global_user_id,
                            ticker,
                        ),
                    )
                # COMMIT

                client.connection.commit()

                # REPLY MSG

                embeddict = {
                    "Sell Successful": ticker,
                    "Shares": str(numshares),
                    "Price": float_beautifier(price, 8 if global_crypto else 2),
                    "Gross Gain": float_beautifier(cost, 8 if global_crypto else 2),
                    "Net": float_beautifier(diff, 8 if global_crypto else 2),
                    "Trader": msg.author.mention,
                }
                rtrnmsg = get_embed(embeddict, stonksurl if diff > 0 else notstonksurl)
                return await utils.delay_send(msg.channel, embed=rtrnmsg)
            elif sep_contents[0].lower() == "net":

                # DB QUERY

                stonks = get_all_shares(client.c, global_user_id)

                # DEFAULT CASH

                if len(stonks) == 0:
                    return await utils.delay_send(
                        msg.channel,
                        embed=get_embed(
                            {
                                "Net Worth": float_beautifier(100000.0),
                                "Trader": msg.author.mention,
                            },
                        ),
                    )
                # CALCULATE NET

                stonks = {s[1]: s[2] for s in stonks}
                totalcash = stonks["MONEY"]

                # NO STOCKS

                if len(stonks) == 0:
                    return await utils.delay_send(
                        msg.channel,
                        embed=get_embed(
                            {
                                "Net Worth": float_beautifier(totalcash),
                                "Trader": msg.author.mention,
                            },
                        ),
                    )
                # PREP API QUERY (BATCH CALL)

                tickers = []
                for stonk in stonks:
                    if stonk != "MONEY":
                        tickers.append(stonk)
                if len(tickers) == 0:
                    return await utils.delay_send(
                        msg.channel,
                        embed=get_embed(
                            {
                                "Net Worth": float_beautifier(totalcash),
                                "Trader": msg.author.mention,
                            },
                        ),
                    )
                # API QUERY

                apistatus, stonkinfo = await get_stock(tickers)
                if not apistatus:
                    raise StockError(
                        "Failed to retrieve stock information: " + stonkinfo
                    )
                # GET PRICES

                for stonk in stonkinfo:
                    totalcash += float(get_price(stonk)) * float(
                        stonks[stonk["symbol"]]
                    )
                # REPLY MSG

                return await utils.delay_send(
                    msg.channel,
                    embed=get_embed(
                        {
                            "Net Worth": float_beautifier(totalcash),
                            "Trader": msg.author.mention,
                        },
                    ),
                )
            elif sep_contents[0].lower() == "portfolio":

                # DB QUERY STOCKS

                stonks = get_all_shares(client.c, global_user_id)

                # CHECK FOR NON CASH

                if len(stonks) <= 1:
                    return await utils.delay_send(
                        msg.channel, msg.author.mention + ", portfolio empty"
                    )
                # PREP REPLY

                tickers = [stonk[1] for stonk in stonks]

                apistatus, getstonks = await get_stock(tickers)
                if not apistatus:
                    raise StockError(
                        "Failed to retrieve stock information: " + stonkinfo
                    )

                values = {stonk["symbol"]: float(get_price(stonk)) for stonk in getstonks}
                
                returns = {}
                for stonk in stonks:
                    if stonk[1] != "MONEY":
                        returns[stonk[1]] = stonk[2] * (values[stonk[1]] - stonk[3])

                embeddict = {
                    stonk[1]: str(float(stonk[2])) + "\n($" + ("+" if returns[stonk[1]] > 0 else "") + float_beautifier(returns[stonk[1]])[1:] + ")"
                    for stonk in stonks
                    if stonk[1] != "MONEY"
                }
                embeddict["Trader"] = msg.author.mention
                rtrnmsg = get_embed(embeddict, wsburl, 0, "Portfolio")

                # REPLY MSG

                return await utils.delay_send(msg.channel, embed=rtrnmsg)
            elif sep_contents[0].lower() == "cash":

                # DB QUERY CASH

                cash = get_existing_shares(client.c, global_user_id, "MONEY")

                # DEFAULT CASH

                if cash == None:
                    return await utils.delay_send(
                        msg.channel,
                        embed=get_embed(
                            {
                                "Cash": float_beautifier(100000.0),
                                "Trader": msg.author.mention,
                            },
                        ),
                    )
                # REPLY MSG

                return await utils.delay_send(
                    msg.channel,
                    embed=get_embed(
                        {
                            "Cash": float_beautifier(float(cash[0])),
                            "Trader": msg.author.mention,
                        },
                    ),
                )
            elif sep_contents[0].lower() == "rank":

                # DB QUERY TICKERS

                allstonks = get_all_shares(client.c)
                if allstonks == None:
                    return await utils.delay_send(msg.channel, "Leaderboard empty")
                # PREP API QUERY

                tickers = list(set([t[1] for t in allstonks if t[1] != "MONEY"]))

                # API QUERY

                apistatus, stonkinfo = await get_stock(tickers)
                if not apistatus:
                    raise StockError(
                        "Failed to retrieve stock information: " + stonkinfo
                    )
                # CALCULATE

                values = {
                    stonk["symbol"]: float(get_price(stonk)) for stonk in stonkinfo
                }

                userdict = {}
                for stonk in allstonks:
                    if stonk[0] in userdict:
                        userdict[stonk[0]] += stonk[2] * (
                            values[stonk[1]] if stonk[1] != "MONEY" else 1
                        )
                    else:
                        userdict[stonk[0]] = stonk[2] * (
                            values[stonk[1]] if stonk[1] != "MONEY" else 1
                        )
                # SORT

                userdict = dict(
                    reversed(sorted(userdict.items(), key=lambda item: item[1]))
                )

                # PREP EMBED

                embeddict = {}
                i = 1
                for user in userdict:
                    name = client.get_user(user)
                    if name != None:
                        embeddict[
                            str(i)
                        ] = name.mention + "\n" + float_beautifier(userdict[user])
                        i += 1
                    if i == 11:
                        break
                rtrnmsg = get_embed(embeddict, wsburl, 0, "Top 10 Traders")

                # REPLY MSG

                return await utils.delay_send(msg.channel, embed=rtrnmsg)
            elif sep_contents[0].lower() == "reset":
                stonks = get_all_shares(client.c, global_user_id)

                if len(stonks) == 0:
                    raise StockError("Net worth not low enough")

                if len(stonks) > 1:
                    raise StockError("All equity must be sold prior to reset")

                if stonks[0][2] > 50000.0:
                    raise StockError("Net worth not low enough")
                else:
                    update_shares(client.c, (100000.0, 0.0, global_user_id, "MONEY"))
                    client.connection.commit()
                    
                    return await utils.delay_send(msg.channel, msg.author.mention + ", your trade account has been reset")
            else:
                raise StockError("Usage: " + self.usage)
        except StockError as se:
            return await utils.delay_send(msg.channel, str(se))
